
Windows IP Configuration

   Host Name . . . . . . . . . . . . : L6230-05
   Primary Dns Suffix  . . . . . . . : 
   Node Type . . . . . . . . . . . . : Hybrid
   IP Routing Enabled. . . . . . . . : No
   WINS Proxy Enabled. . . . . . . . : No

Ethernet adapter Local Area Connection:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : 
   Description . . . . . . . . . . . : Intel(R) 82579LM Gigabit Network Connection
   Physical Address. . . . . . . . . : B8-CA-3A-C5-D1-53
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes

Wireless LAN adapter Local Area Connection* 1:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : 
   Description . . . . . . . . . . . : Microsoft Wi-Fi Direct Virtual Adapter
   Physical Address. . . . . . . . . : 24-77-03-C0-FD-35
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes

Wireless LAN adapter Local Area Connection* 2:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : 
   Description . . . . . . . . . . . : Microsoft Wi-Fi Direct Virtual Adapter #2
   Physical Address. . . . . . . . . : 26-77-03-C0-FD-34
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes

Wireless LAN adapter Wireless Network Connection:

   Connection-specific DNS Suffix  . : 
   Description . . . . . . . . . . . : Intel(R) Centrino(R) Ultimate-N 6300 AGN
   Physical Address. . . . . . . . . : 24-77-03-C0-FD-34
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
   IPv6 Address. . . . . . . . . . . : 2409:4042:2383:7c81:5888:ea54:dabd:a26(Preferred) 
   Temporary IPv6 Address. . . . . . : 2409:4042:2383:7c81:217c:fee:2538:6f49(Preferred) 
   Link-local IPv6 Address . . . . . : fe80::5888:ea54:dabd:a26%4(Preferred) 
   IPv4 Address. . . . . . . . . . . : 192.168.43.168(Preferred) 
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Lease Obtained. . . . . . . . . . : Friday, September 20, 2019 9:26:35 PM
   Lease Expires . . . . . . . . . . : Saturday, September 21, 2019 12:56:15 AM
   Default Gateway . . . . . . . . . : fe80::d92d:457f:2ec2:85cb%4
                                       fe80::b46e:e232:1719:19c0%4
                                       192.168.43.1
   DHCP Server . . . . . . . . . . . : 192.168.43.1
   DHCPv6 IAID . . . . . . . . . . . : 304379651
   DHCPv6 Client DUID. . . . . . . . : 00-01-00-01-23-D0-B1-FD-B8-CA-3A-C5-D1-53
   DNS Servers . . . . . . . . . . . : 2405:200:800::1
                                       192.168.43.1
   NetBIOS over Tcpip. . . . . . . . : Enabled

Ethernet adapter Bluetooth Network Connection:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . : 
   Description . . . . . . . . . . . : Bluetooth Device (Personal Area Network)
   Physical Address. . . . . . . . . : 20-68-9D-5E-DC-FB
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
fullstamp: "2019-09-21_00-07-21" 
